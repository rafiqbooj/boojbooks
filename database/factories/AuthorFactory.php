<?php

namespace Database\Factories;

use App\Models\Author;
use Illuminate\Database\Eloquent\Factories\Factory;

class AuthorFactory extends Factory
{
    public $timestamps = false;

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Author::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'thumbnail' => $this->faker->imageUrl(),
            'user_id' => 1,
        ];
    }

    /**
     * Assign a random user id.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function randomUser($max_id)
    {
        return $this->state(function () use ($max_id) {
            return [
                'user_id' => $this->faker->numberBetween(1, $max_id),
            ];
        });
    }
}
