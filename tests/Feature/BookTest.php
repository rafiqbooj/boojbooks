<?php

namespace Tests\Feature;

use App\Models\Book;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Nuwave\Lighthouse\Testing\MakesGraphQLRequests;
use Tests\TestCase;

class BookTest extends TestCase
{
    use RefreshDatabase, MakesGraphQLRequests;

    protected function setUp(): void
    {
        parent::setUp();


    }

    public function test_it_returns_a_list_of_books()
    {
        $user = User::factory()->create();
        $this->be($user);

        Book::factory()->count(10)->create();

        $response = $this->graphQL(/** @lang GraphQL */ '
        {
            books {
                data {
                    id
                    name
                    author
                    description
                    thumbnail
                }
            }
        }
        ');

        $response->assertStatus(200)
            ->assertJsonCount(10, 'data.books.data')
            ->assertJsonStructure([
                'data' =>
                    ['books' => [
                        'data' => [
                            '*' => [
                                'id',
                                'name',
                                'author',
                                'description',
                                'thumbnail',
                            ],
                        ],
                    ],
                ],
            ]);
    }

    public function test_it_only_returns_user_books()
    {
        $otherUser = User::factory()->create();

        Book::factory()->count(5)->create(['user_id' => $otherUser]);

        $user = User::factory()->create();
        $this->be($user);

        Book::factory()->count(5)->create();

        $response = $this->graphQL(/** @lang GraphQL */ '
        {
            books {
                data {
                    name
                }
            }
        }
        ');

        $response->assertStatus(200)
            ->assertJsonCount(5, 'data.books.data');
    }
}
