<?php

namespace App\Observers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class UserIdObserver
{
    /**
     * Handle the "creating" event.
     *
     * @param  Model  $model
     * @return void
     */
    public function creating(Model $model)
    {
        if (Auth::check()) {
            $model->user_id = Auth::id();
        }
    }
}
