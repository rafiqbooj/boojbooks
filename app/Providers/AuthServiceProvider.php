<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // automatically login as user id 1
        if(!App::runningInConsole() && env('BYPASS_LOGIN') === true) {
            $authUser = User::where(['id' => 1])->firstOr(function() {
                return User::factory(1)->create(['id' => 1]);
            });
            Auth::login($authUser);
        }
    }
}
