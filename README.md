## Booj Books
This is a basic Laravel api providing REST authentication endpoints (using Fortify/Sanctum), and a graphql interface for querying books and authors (using Lightouse).

## Setup Instructions
You can use your preferred dev environment (homestead/valet) or use the included docker-compose.yml (in the docker directory) which will spin up phpdocker/nginx & mysql and forward their respective ports.

To bash into the container to run artisan/composer commands you can run this command: **docker-compose exec php-fpm bash**

Copy .env.example to .env and run composer install and then run migrations. You can optionall run seeds to add some data.

Until the frontend is ready, you can bypass login/auth (and automatically be logged in as user id 1) by setting BYPASS_LOGIN to true in your environmental vars.

For making graphql queries you can use the included client at /graphql-playground (which includes the schema definition/autocompletion), or if you prefer to use Postman there's a collection in the repo base directory.

The graphql api is defined by the schema file in the graphql directory.

There's a couple basic tests included.

## Assignment
Implement Tags into the graphql api. Users should be able to create/update/delete tags, along with tagging books and authors. Include tests if time permits.
